module gitee.com/ling-bin/repository

go 1.16

require (
	gitee.com/ling-bin/go-utils v1.6.23
	github.com/apache/rocketmq-client-go/v2 v2.0.0
	github.com/go-redis/redis/v8 v8.11.5
	github.com/json-iterator/go v1.1.12
	github.com/olivere/elastic/v7 v7.0.22
	github.com/seefan/gossdb v1.1.2 // indirect
	github.com/seefan/gossdb/v2 v2.0.0
	go.mongodb.org/mongo-driver v1.10.3 //驱动地址：https://github.com/mongodb/mongo-go-driver
)
