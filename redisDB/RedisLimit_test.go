package redisDB

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestRedislimit_IsLimit(t *testing.T) {
	count := 12
	key := "234234"
	var wg sync.WaitGroup
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func(index int) {
			defer func() {
				wg.Done()
			}()
			redisLimit, err := createRedisLimit()
			if err != nil {
				fmt.Println("限速器初始化异常")
				return
			}
			ok,err:= redisLimit.IsLimit(key,7,time.Minute)
			if err != nil {
				fmt.Println("限速器设置异常")
				return
			}
			if !ok {
				fmt.Println("未限速")
			} else {
				_,err := redisLimit.DelLimit(key)
				if err != nil{
					fmt.Println("清理限速资源失败！")
				}

				fmt.Println("限速")
			}
		}(i)
	}
	wg.Wait()
}

func mapping2()(*RedisContent,error) {
	config := make(map[string]string, 5)
	config["ipport"] = "192.168.1.24:6379"
	config["timeout"] = "30" //秒
	// 获取mongo实例连接
	return DataBaseMapping("mapping1", func(mappingName string) map[string]string {
		config := make(map[string]string, 5)
		config["ipport"] = "192.168.1.24:6379"
		config["timeout"] = "30" //秒
		return config
	}, func() map[string]int {
		return map[string]int{}
	})
}

//创建用户仓储
func createRedisLimit() (*RedisLimit, error) {
	mapping, err := mapping2()
	if err != nil {
		return nil, err
	}
	return mapping.GetRedisLimit(),nil
}