package redisDB

import (
	"github.com/go-redis/redis/v8"
)

// IRedisRepository 仓储接口(只支持string类型)
type IRedisRepository interface {

	//Set 插入单个
	// entity  结构体
	// ttl     单位秒 ，0为永不到期
	Set(key string, entity interface{}, ttl int64) (int64, error)

	//SetMany 插入多个【单次20000个以下】
	// entityMap 结构体字典
	SetMany(entityMap map[string]interface{}) (int64, error)

	//Incr 递增字段值
	//key hashid
	//fieid 字段名称
	Incr(key ,field string,num int64) (int64,error)

	//GetIncr 获取递增字段值
	//key hashid
	//res 要获取的字段值
	GetIncr(key string,res map[string]int64) (err error)

	//RemoveIncr 移除递增
	//key hashid
	RemoveIncr(key string) error

	//Remove 删除单个
	// key key值
	Remove(key ...string) (int64, error)

	//Get 查询单个
	// key         key值
	// result      查询的结果
	Get(key string, result interface{}) error

	//GetMany 查询多个[只支持string类型]
	// keys        key值切片
	// result      查询的结果切片
	GetMany(keys []string, result interface{}) error

	// CreateKey 创建内部key（配合直接使用数据库连接（GetClient）使用,其它方法不需要使用内部已调用）
	// prefix 前缀
	// key
	CreateKey(prefix,key string) string

	//GetClient 获取客户端
	//GetClient() redis.Cmdable

	//Do 直接执行
	Do(f func(redis.Cmdable))
}
