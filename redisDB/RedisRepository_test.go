package redisDB

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"testing"
	"time"
)

var key = "densRedisTest"

//测试结构体
type TestRed struct {
	Id   int64  `json:"id"`
	Name string `json:"name"` //名称
	Age  int    `json:"age"`  //年龄
}

func TestReidsRepository_Set(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}

	count, err := repository.Set(key,&TestRed{
		Id:   time.Now().Unix() ,
		Name: "测试",
		Age:  20,
	},0)
	if err != nil {
		panic(fmt.Sprint("插入数据异常:", err))
		return
	}

	fmt.Println("插入数据条数:",count,"条")
}

func TestReidsRepository_SetMany(t *testing.T) {
	count := 10
	var wg = sync.WaitGroup{}
	wg.Add(count)
	for j := 0 ; j < count;j++ {
		go func(index int) {
			repository, err := createTestRedRepository()
			if err != nil {
				panic(fmt.Sprint("数据库连接异常:", err))
				return
			}
			dataMap := make(map[string]interface{})
			for i := 0; i < 1000; i++ {
				dataMap[fmt.Sprint(key, ":", i, ":", index)] = &TestRed{
					Id:   time.Now().Unix(),
					Name: "测试:" + strconv.Itoa(i)+":"+ strconv.Itoa(index),
					Age:  20,
				}
			}
			result, err := repository.SetMany(dataMap)
			if err != nil {
				panic(fmt.Sprint("设置数据异常:", err))
				return
			}
			fmt.Println("设置数据多个:", result)
			wg.Done()
		}(j)
	}
	wg.Wait()
}

func TestReidsRepository_Incr(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常！"))
		return
	}
	incr, err := repository.Incr(key, "count", 1)
	if err != nil {
		panic(fmt.Sprint("递增数据异常:", err))
		return
	}
	fmt.Println("递增后值:", incr)
}

func TestReidsRepository_GetIncr(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常！"))
		return
	}
	res := map[string]int64{
		"count": 0,
	}
	err = repository.GetIncr(key, res)
	if err != nil {
		panic(fmt.Sprint("获取递增数据异常:", err))
		return
	}
	fmt.Println("获取递增数据:")
	fmt.Println(res)
}

func TestReidsRepository_RemoveIncr(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	err = repository.RemoveIncr(key)
	if err != nil {
		panic(fmt.Sprint("移除递增数据异常:", err))
		return
	}
	fmt.Println("递增已经移除")
	res := map[string]int64{
		"count": 0,
	}
	err = repository.GetIncr(key, res)
	if err != nil {
		panic(fmt.Sprint("获取递增数据异常:", err))
		return
	}
	fmt.Println("获取递增数据:")
	fmt.Println(res)
}


func TestReidsRepository_Get(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	result := &TestRed{}
	err = repository.Get("123", result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据单个:", result)
}


func TestReidsRepository_GetMany(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	result := make([]*TestRed, 0, 10)
	err = repository.GetMany([]string{key, ""}, &result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据多个:")
	fmt.Println(result)
}

func TestReidsRepository_Remove(t *testing.T) {
	repository, err := createTestRedRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	count, err := repository.Remove(key)
	if err != nil {
		panic(fmt.Sprint("删除数据异常:", err))
		return
	}
	fmt.Println("删除数据条数:", count, "条")
}


//创建用户仓储
func createTestRedRepository() (IRedisRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	repository,ok := mapping.GetRedisRepository("TestRed")
	if !ok {
		return nil,errors.New("获取表异常")
	}
	return repository, nil
}

func Mapping()(*RedisContent,error) {
	// 获取mongo实例连接
	return DataBaseMapping("Mapping", func(mappingName string) map[string]string {
		config := make(map[string]string, 5)
		config["ipport"] = "192.168.1.24:6379"
		config["timeout"] = "30"  //秒
		return config
	}, func() map[string]int {
		return map[string]int{
			"TestRed":1,
		}
	})
}