package redisDB

import (
	"fmt"
	"gitee.com/ling-bin/go-utils/pools"
	"testing"
	"time"
)


//TestRedisLock_Lock 测试分布式锁
func TestRedisLock_Lock(t *testing.T) {
	count := int64(0)
	tCount := 100000
	timeTaskPool := pools.NewTaskWorkerPool("",32,10000)
	timeTaskPool.StartWorkerPool(func(errString string) {
		fmt.Println("运行异常：",errString)
	})
	lockKey := "lockId:234234234"
	for i := 0 ;i < tCount ; i++{
		timeTaskPool.SendRunFnToTaskQueueWait(func() {
			redisLock, _ := createRedisLock(lockKey)
			isOk, err := redisLock.TryLock(time.Second*5, time.Second*10)
			if err == nil && isOk {
				count++
				redisLock.ULock()
			}
		})
	}
	for timeTaskPool.GetTotalHandleCount() != int64(tCount) {
		time.Sleep(time.Second)
		fmt.Println("计算中。。。 ",count)
	}
	fmt.Println("累加值：",count," 期望值：",tCount,"  是否处理ok：", count == int64(tCount))
}


func mapping1()(*RedisContent,error) {
	// 获取mongo实例连接
	return DataBaseMapping("mapping1", func(mappingName string) map[string]string {
		config := make(map[string]string, 5)
		config["ipport"] = "192.168.1.34:6379"
		config["timeout"] = "30" //秒
		return config
	}, func() map[string]int {
		return map[string]int{}
	})
}


//创建用户仓储
func createRedisLock(lockKey string) (*RedisLock, error) {
	mapping, err := mapping1()
	if err != nil {
		return nil, err
	}
	return mapping.GetRedisLock(lockKey),nil
}