package RocketMQ

import (
	"context"
	"fmt"
	"github.com/apache/rocketmq-client-go/v2"
	"github.com/apache/rocketmq-client-go/v2/consumer"
	"github.com/apache/rocketmq-client-go/v2/primitive"
	"github.com/apache/rocketmq-client-go/v2/producer"
	"testing"
	"time"
)

var (
	groupName = "group1"
	topic ="test_topic"
	tags = "tags1"
	MqRetryTimes = 3
)

func TestQ(t *testing.T) {
	go tProducer()
	go tConsumer("1")
	go tConsumer("2")
	go tConsumer("3")
	tConsumer("4")
}

//生产者
func tProducer() {
	nameServers:= []string{"127.0.0.1:9876"}
	mqProducer, err := rocketmq.NewProducer(
		producer.WithNameServer(nameServers),
		producer.WithInstanceName("NameSev"),
		producer.WithGroupName(groupName), //发送一个消息多个组都可以收到消息，同一个组内只能有一个消费者消费到消息
		producer.WithRetry(MqRetryTimes),
		//顺序消息
		//producer.WithQueueSelector(producer.NewHashQueueSelector()),
	)
	if err != nil {
		fmt.Printf("init rocket mq producer err:%v", err)
		return
	}
	err = mqProducer.Start()
	if err != nil {
		fmt.Printf("producer mq start err:%v", err)
		return
	}
	for {
		msg := primitive.NewMessage(topic, []byte("22222222222222"))
		msg = msg.WithTag(tags)

		//顺序消息
		//msg = msg.WithShardingKey("1024")

		result, err := mqProducer.SendSync(context.Background(), msg)
		if err != nil {
			fmt.Println("消息发送异常：", err)
		} else {
			fmt.Println("消息发送结果：", result)
		}
		time.Sleep(time.Second)
	}
}

//消费者
func tConsumer(groupName string) {
	nameServers := []string{"127.0.0.1:9876"}
	mqPushConsumer, err := rocketmq.NewPushConsumer(
		consumer.WithNameServer(nameServers),
		consumer.WithInstance("NameSev"),
		consumer.WithGroupName(groupName), //发送一个消息多个组都可以收到消息，同一个组内只能有一个消费者消费到消息
	)
	if err != nil {
		fmt.Printf("init rocket mq Consumer err:%v", err)
		return
	}
	selector := consumer.MessageSelector{
		Type:       consumer.TAG,
		Expression: tags,   //发送消息带有tags 可以根据tags过滤消费消息
	}
	mqPushConsumer.Subscribe(topic, selector, func(ctx context.Context, ext ...*primitive.MessageExt) (consumer.ConsumeResult, error) {
		for _, msg := range ext {
			fmt.Println("收到消息[", groupName, "]:", string(msg.Body))
		}
		return consumer.ConsumeSuccess, nil
	})
	//启动前先订阅
	err = mqPushConsumer.Start()
	if err != nil {
		fmt.Printf("Consumer mq start err:%v", err)
		return
	}
	select {}
}