package fileStore

import (
	"fmt"
	"testing"
)

func TestStoreClient_Subsection(t *testing.T) {
	store := Filemapping()
	id, err := store.Start("txt", []byte("Start -- "),0)
	if err != nil {
		panic(err)
	}
	for i := 0; i <= 5; i++ {
		store.Continue(id, []byte(fmt.Sprint("|Continue:", i, " --|")))
	}
	path, err := store.Complete(id, []byte(fmt.Sprint("|Complete: --|")),nil)
	if err != nil {
		panic(err)
	}
	fmt.Println(path)
}


var fileName = "123.txt"
func TestStoreClient_Upload(t *testing.T) {
	store := Filemapping()
	upload, err := store.Upload(".txt", []byte("测试"))
	if err != nil {
		panic(fmt.Sprint("上传失败:", err))
		return
	}
	fmt.Println("上传成功：", upload)
	fileName = upload
}

func TestStoreClient_Delete(t *testing.T) {
	store := Filemapping()
	b, err := store.Delete(fileName)
	if !b {
		panic(fmt.Sprint("文件删除失败：", err))
		return
	}
	fmt.Println("删除成功:", b)
}

//go-fastdfs
func Filemapping() IStoreClient {
	return GetStoreClient("filemapping", func(mappingName string) map[string]string {
		config := make(map[string]string, 3)
		config["groupname"] = "group1"
		config["ipport"] = "192.168.1.24:8080"
		return config
	})
}