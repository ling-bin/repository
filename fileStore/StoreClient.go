package fileStore

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ling-bin/go-utils/idCounter"
	"gitee.com/ling-bin/repository/appCache"
	"net"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"

	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"time"
)

//文件存储客户端字典
var (
	storeClientMap = sync.Map{}
	lock             sync.Mutex // 连接锁定防止并发访问
	dirPath          string
	repository     = appCache.GetCacheRepository("FileData")
)

func init()  {
	dirPath, _ = os.Getwd()
}

//StoreClient 文件上传客户端
type StoreClient struct {
	scene      string        //场景(可以按这个分类)
	uploadUrl  string        //上传地址
	deleteUrl  string        //删除地址
	timeout    time.Duration //超时时间
	httpClient *http.Client  //http客户端
}

//GetStoreClient 获取文件上传客户端
func GetStoreClient(mappingName string,configFn func(mappingName string)map[string]string) IStoreClient {
	val, ok := storeClientMap.Load(mappingName)
	if ok {
		return val.(IStoreClient)
	}
	lock.Lock()
	defer lock.Unlock()
	val, ok = storeClientMap.Load(mappingName)
	if ok {
		return val.(IStoreClient)
	}

	config := configFn(mappingName)
	ipPort := config["ipport"]
	group := config["groupname"]
	//请求组织
	basicStr := fmt.Sprint("http://",ipPort,"/",group) //请求地址
	timeout := time.Second * 30
	if timeoutStr, ok := config["timeout"]; ok {
		atoi, err := strconv.Atoi(timeoutStr)
		if err == nil {
			timeout = time.Second * time.Duration(atoi)
		}
	}
	//场景
	scene,ok := config["scene"]
	if !ok {
		scene = "default"
	}
	client := newStoreClient(basicStr, scene, timeout)
	storeClientMap.Store(mappingName, client)
	return client
}

//newStoreClient 实例化文件上传客户端
//ipport ip和端口
//group  组名称（可以按照这个分集群）
//scene  场景(可以按这个分类)
//timeout 请求超时时间
func newStoreClient(basicStr string, scene string, timeout time.Duration) IStoreClient {
	newClient := &StoreClient{
		scene:     scene,
		uploadUrl: fmt.Sprint(basicStr, "/upload"),
		deleteUrl: fmt.Sprint(basicStr, "/delete?path="),
		timeout:   timeout,
		httpClient: &http.Client{
			Timeout: timeout,
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   timeout,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				DisableCompression:    true,
			},
		},
	}
	return newClient
}

//fileDataInfo 文件数据
type fileDataInfo struct {
	fileName string          //文件名
	file     *os.File        //文件描述符
	writer   *bufio.Writer   //写缓存
}

//GetFilePath  文件路径
func (c *StoreClient) GetFilePath(fileName string) string {
	return fmt.Sprint(dirPath, "/", fileName)
}

//Start 开始文件上传[本地缓存]
//extName 文件扩展名【不带. 如 jpg mp3】
//data    文件数据块
//index  文件段序号（传小于0内部默认按插入顺序从0开始）序号一样会覆盖
//ttl     应用程序缓存时间[秒]
//id      返回的索引
//error   数据或扩展名不对时
func (c *StoreClient) Start(extName string, data []byte, ttl int64) (id string, err error) {
	if len(data) == 0 {
		return "", errors.New("file len not 0")
	}
	if extName == "" {
		return "", errors.New("extName err")
	}
	fileName := fmt.Sprint(idCounter.NewObjectID().Hex(), ".", extName)
	filePath := c.GetFilePath(fileName)
	file, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE, 0777)
	if err != nil {
		return "", err
	}
	writer := bufio.NewWriterSize(file,32*1024)
	fileData := &fileDataInfo{
		fileName: fileName,
		file:     file,
		writer:   writer,
	}
	writer.Write(data)
	err = writer.Flush()
	if err != nil {
		file.Close()
		return "", err
	}
	//设置到缓存
	repository.Set(fileName, fileData, 10*60, c.clearFile, 10)
	return fileName, nil
}

//clearFile清理文件
func (c *StoreClient) clearFile(fileDataObj interface{}, count int64) {
	defer func() {
		if r :=recover();r != nil{
			return
		}
	}()
	fileData := fileDataObj.(*fileDataInfo)
	fileData.writer.Flush()
	fileData.file.Close()
	filePath := c.GetFilePath(fileData.fileName)
	os.Remove(filePath)
}

//Continue id    开始返回的索引
//file    文件数据块【不可为空】
//index   文件段序号（传小于0内部默认按插入顺序从0开始）序号一样会覆盖
//error   本地无缓存或缓存失效时
func (c *StoreClient) Continue(id string, data []byte) (err error) {
	fileDataObj,err := repository.Get(id)
	if err != nil {
		return err
	}
	fileData := fileDataObj.(*fileDataInfo)
	fileData.writer.Write(data)
	err = fileData.writer.Flush()
	if err != nil {
		return err
	}
	return nil
}

//Complete id      开始返回的索引
//file    文件数据块【可为空】
//index  文件段序号（传小于0内部默认按插入顺序从0开始）序号一样会覆盖
//error   本地无缓存或缓存失效时
//hf      (完整文件数据，上传的包) (要保存的数据，是否现在保存:false不保存待后续数据 true立刻保存)   【可为空】
func (c *StoreClient) Complete(id string, data []byte, hf func(string) string) (path string, err error) {
	fileDataObj, err := repository.Get(id)
	if err != nil {
		return "", err
	}
	fileData := fileDataObj.(*fileDataInfo)
	fileData.writer.Write(data)
	err = fileData.writer.Flush()
	if err != nil {
		fileData.file.Close()
		return "", err
	}
	if err != nil {
		fileData.file.Close()
		return "", err
	}
	fileData.file.Close()
	filePath := c.GetFilePath(fileData.fileName)
	defer os.Remove(filePath)
	if hf != nil {
		newFilePath := hf(filePath)
		if newFilePath != filePath {
			defer os.Remove(newFilePath)
		}
		upload, err := c.upload(newFilePath)
		if err == nil {
			repository.Remove(id)
		}
		return upload, err
	} else {
		upload, err := c.upload(filePath)
		if err == nil {
			repository.Remove(id)
		}
		return upload, err
	}
}

//upload 本地文件上传
//fileName 文件名称
//filePath 文件路径
func (c *StoreClient) upload(filePath string) (newFilePath string, err error) {
	fileName := path.Base(filePath)
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", fileName)
	if err != nil {
		return "", err
	}
	file, err := os.OpenFile(filePath, os.O_RDWR, 0777)
	if err != nil {
		return "", err
	}
	defer file.Close()

	reader := bufio.NewReaderSize(file, 16*1024)
	_, err = io.Copy(part, reader)
	// 其他参数列表写入 body
	writer.WriteField("scene", c.scene)
	writer.WriteField("output", "json2")
	if err := writer.Close(); err != nil {
		return "", err
	}
	request, err := http.NewRequest("POST", c.uploadUrl, body)
	if err != nil {
		return "", err
	}
	// 添加请求头
	request.Header.Add("Content-Type", writer.FormDataContentType())
	request.Header.Add("Connection", "keep-alive")
	request.Header.Add("Accept-Encoding", "gzip")
	// 发送请求
	response, err := c.httpClient.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	//请求状态
	if response.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return "", fmt.Errorf("Read body err: %s - %s - %s ", response.Status, err, string(body))
		}
		return "", fmt.Errorf("server response: %s ", response.Status)
	}
	respBody := response.Body
	//解压
	if response.Header.Get("Content-Encoding") == "gzip" {
		respBody, err = gzip.NewReader(respBody)
		if err != nil {
			return "", err
		}
	}
	bodyByte, err := ioutil.ReadAll(respBody)
	if err != nil {
		return "", fmt.Errorf("Read body err: %s - %s ", err, string(bodyByte))
	}
	result := &ResultData{}
	err = json.Unmarshal(bodyByte, result)
	if err != nil {
		return "", err
	}
	if result.Status != "ok" {
		return "", fmt.Errorf("Read body err: %s - %s ", err, string(bodyByte))
	}
	return result.Data.Path, nil
}

//Upload   文件数据上传
//extName  文件扩展名【不带. 如 jpg mp3】
//file     完整数据
func (c *StoreClient) Upload(extName string, data []byte) (newFilePath string, err error) {
	objectID := idCounter.NewObjectID().Hex()
	if strings.Index(extName, ".") > -1 {
		extNameAry := strings.Split(extName, ".")
		extName = extNameAry[len(extNameAry)-1]
	}
	fileName := fmt.Sprint(objectID, ".", extName)
	body := &bytes.Buffer{}
	// 文件写入 body
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", fileName)
	if err != nil {
		return "", err
	}
	reader := bytes.NewReader(data)
	_, err = io.Copy(part, reader)
	// 其他参数列表写入 body
	writer.WriteField("scene", c.scene)
	writer.WriteField("output", "json2")
	if err := writer.Close(); err != nil {
		return "", err
	}
	request, err := http.NewRequest("POST", c.uploadUrl, body)
	if err != nil {
		return "", err
	}
	// 添加请求头
	request.Header.Add("Content-Type", writer.FormDataContentType())
	request.Header.Add("Connection", "keep-alive")
	request.Header.Add("Accept-Encoding", "gzip")
	// 发送请求
	response, err := c.httpClient.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	//请求状态
	if response.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return "", fmt.Errorf("Read body err: %s - %s - %s ", response.Status, err, string(body))
		}
		return "", fmt.Errorf("server response: %s ", response.Status)
	}
	respBody := response.Body
	//解压
	if response.Header.Get("Content-Encoding") == "gzip" {
		respBody, err = gzip.NewReader(respBody)
		if err != nil {
			return "", err
		}
	}
	bodyByte, err := ioutil.ReadAll(respBody)
	if err != nil {
		return "", fmt.Errorf("Read body err: %s - %s ", err, string(bodyByte))
	}
	result := &ResultData{}
	err = json.Unmarshal(bodyByte, result)
	if err != nil {
		return "", err
	}
	if result.Status != "ok" {
		return "", fmt.Errorf("Read body err: %s - %s ", err, string(bodyByte))
	}
	return result.Data.Path, nil
}

//Delete 文件删除
func (c *StoreClient) Delete(path string) (ok bool, err error) {
	deleteUrl := strings.Builder{}
	deleteUrl.WriteString(c.deleteUrl)
	deleteUrl.WriteString(path)
	//请求组织
	httpRequest, err := http.NewRequest("Get", deleteUrl.String(), nil)
	if err != nil {
		return false, nil
	}
	// 发送请求
	resp, err := c.httpClient.Do(httpRequest)
	if err != nil {
		return false, nil
	}
	defer resp.Body.Close()
	response, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println(string(response))
	result := &ResultData{}
	err = json.Unmarshal(response, result)
	if err != nil {
		return false, err
	}
	if result.Status != "ok" {
		return false, nil
	}
	return true, nil
}