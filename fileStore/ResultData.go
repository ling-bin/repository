package fileStore

//ResultData 请求结果数据
type ResultData struct {
	Data    FileData `json:"data"`    //数据
	Message string   `json:"message"` //消息
	Status  string   `json:"status"`  //状态
}

//FileData 文件信息
type FileData struct {
	Domain  string `json:"domain"`  // 域名
	Md5     string `json:"md5"`     // md5
	Mtime   int64  `json:"mtime"`   // 时间戳秒
	Path    string `json:"path"`    // 路径
	Retcode int    `json:"retcode"` // 响应码
	Retmsg  string `json:"retmsg"`  // 响应内容
	Scene   string `json:"scene"`   // 上传到的场景
	Scenes  string `json:"scenes"`  // 支持的场景
	Size    int    `json:"size"`    // 文件大小
	Src     string `json:"src"`     // 文件地址
	Url     string `json:"url"`     // 文件访问地址
}
