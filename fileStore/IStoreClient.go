package fileStore

//IStoreClient 文件存储客户端
type IStoreClient interface {

	//Start 开始文件上传[本地缓存]
	//extName 文件扩展名【不带. 如 jpg mp3】
	//data    文件数据块【不可为空】
	//ttl     应用程序缓存时间[秒]
	//id      返回的索引
	//error   数据或扩展名不对时
	Start(extName string, data []byte, ttl int64) (id string, err error)

	//Continue 继续
	//id      开始返回的索引
	//data    文件数据块【不可为空】
	//error   本地无缓存或缓存失效时
	Continue(id string, data []byte) (err error)

	//Complete 完成
	//id      开始返回的索引
	//data    文件数据块【可为空】
	//error   本地无缓存或缓存失效时
	Complete(id string, data []byte, hf func(string)string)(path string, err error)

	//Upload 文件上传
	//extName  文件扩展名【不带. 如 jpg mp3】
	//data     完整数据
	//path     返回 文件路径
	Upload(extName string, data []byte) (path string, err error)

	//Delete 文件删除
	//path     文件路径
	//ok       是否删除成功
	Delete(path string) (ok bool, err error)
}