package TDengineDB

//ITDengineTable 表接口
type ITDengineTable interface {
	GetSuperTableName() string //超级表名称
	GetTagColumn() string      //tag 列名和类型的定义如：accountName binary(64),...
	GetFieldColumn() string    //内容字段的列名和类型定义如：name binary(64),...
}
