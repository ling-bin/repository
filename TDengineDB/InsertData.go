package TDengineDB

// InsertData 插入时表数据
type InsertData struct {
	Tags string   //标签（设备属性除时间外的索引,注同一个表一个标签值是一样的）"Tags":"tag0,tag1...",
	Data []string //数据内容(数据库多行数据) "data":["\"1538548695000, 12.6, 218, 0.33\",""\"1538548695000, 12.6, 218, 0.33\"...]
}