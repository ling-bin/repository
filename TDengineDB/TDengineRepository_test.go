package TDengineDB

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestTDengineRepository_InsertOne(t *testing.T) {
	repository, err := createTDengineRepository()
	if err != nil {
		fmt.Println("数据库连接异常：",err)
		return
	}
	value := &TDenginTest{
		Ts:        time.Now(),
		AddTime:   time.Now().Add(time.Millisecond * time.Duration(1)),
		Account:   "Account_50_2",
		Name:      "desn_n",
		Addr:      "深圳龙岗_n",
		Heartbeat: 10.0,
		Age:       21,
	}
	many, err := repository.Add(value.GetTableName(),value.GetValues(),value.GetTags())
	if err != nil {
		fmt.Println("插入数据异常：", err)
		return
	}
	fmt.Println("插入数据：", many, "条")
}


func TestTDengineRepository_InsertManyt(t *testing.T) {
	count1 := 5000
	count := 1000
	tCount := 200
	addCount := int64(0)

	var wg sync.WaitGroup
	wg.Add(tCount)
	for z := 0; z < tCount; z++ {
		go func(index int) {
			defer func() {
				wg.Done()
			}()
			for i := 0; i < count1; i++ {
				repository, err := createTDengineRepository()
				if err != nil {
					fmt.Println("数据库连接异常：", err)
					return
				}
				dataMap := make(map[string]*InsertData, count)
				for j := 0; j < count; j++ {
					value := &TDenginTest{
						Ts:        time.Now().Add(time.Millisecond * time.Duration(j)),
						AddTime:   time.Now().Add(time.Millisecond * time.Duration(j)),
						Account:   fmt.Sprint("Account_", strconv.Itoa(index), "_", strconv.Itoa(i)),
						Name:      fmt.Sprint("desn_", strconv.Itoa(j)),
						Addr:      fmt.Sprint("深圳龙岗", strconv.Itoa(j)),
						Heartbeat: 10.0 + float64(j),
						Age:       21 + j,
					}
					val := value.GetValues()
					tag := value.GetTags()
					data, ok := dataMap[value.GetTableName()]
					if !ok {
						dataMap[value.GetTableName()] = &InsertData{
							Tags: tag,
							Data: []string{val},
						}
					} else {
						data.Data = append(data.Data, val)
					}
				}
				many, err := repository.AddMany(dataMap)
				if err != nil {
					fmt.Println("插入数据异常：", err)
					return
				}
				atomic.AddInt64(&addCount, many)
			}
		}(z)
	}
	ex := make(chan struct{},1)
	go func() {
		for  {
			select {
			case <-ex:
				return
			default:
				fmt.Println("插入条数：", addCount)
				time.Sleep(time.Second * 2)
			}
		}
	}()
	wg.Wait()
	ex<- struct{}{}
	fmt.Println("插入条数：", addCount)
}

func TestTDengineRepository_QuerySuperPage(t *testing.T) {
	repository, err := createTDengineRepository()
	if err != nil {
		panic(fmt.Sprint("[分页]数据库连接异常：", err))
		return
	}
	queryWhere := "accountName='Account_n' or accountName='Account_0_26'"
	data, totalCount, err := repository.QuerySuperPage("*", queryWhere, 1, 1000, 1)
	if err != nil {
		panic(fmt.Sprint("[分页]数据转换异常：", err))
		return
	}
	mapData,err := data.ToSliceMap()
	if err != nil{
		fmt.Println(err)
	}
	fmt.Println("数据条数_map：", len(mapData))
	var tDenginTest []*TDenginTest
	err = data.ToSlice(&tDenginTest)
	if err != nil{
		fmt.Println(err)
	}
	fmt.Println("数据条数_Slice：", len(tDenginTest))
	fmt.Println("[分页]获取数据：", totalCount, "条  当前页条数：", len(tDenginTest))
}

//测试结构体
type TDenginTest struct {
	Ts        time.Time //时间
	Account   string    //账户名
	AddTime   time.Time //添加时间
	Name      string    //名称
	Age       int       //年龄
	Heartbeat float64   //心跳
	Addr      string    //社区地址
}

//超级表名称[必须唯一,用于超级表生成]
func (s *TDenginTest) GetSuperTableName() string {
	return "tdengintest"
}

//获取子表名称[必须唯一,用于子表生成]
func (s *TDenginTest) GetTableName() string {
	return fmt.Sprint("t_", s.Account)
}

//GetTagColumn 标签列[用于超级表生成]
func (s *TDenginTest) GetTagColumn() string {
	return "accountName binary(64),address nchar(100)"
}

//GetFieldColumn 普通列[用于超级表生成]
func (s *TDenginTest) GetFieldColumn() string {
	return "ts timestamp,addtime timestamp, name binary(64), age int,Heartbeat float"
}

//GetTags 获取插入的Tags[用于插入语句生成]
func (s *TDenginTest) GetTags() string {
	return fmt.Sprint("\"", s.Account, "\",\"", s.Addr, "\"")
}

//GetValue 获取插入的Values[用于插入语句生成]
func (s *TDenginTest) GetValues() string {
	value := strings.Builder{}
	value.WriteString("\"")
	value.WriteString(s.Ts.Format(time.RFC3339Nano))
	value.WriteString("\",\"")
	value.WriteString(s.AddTime.Format(time.RFC3339Nano))
	value.WriteString("\",\"")
	value.WriteString(s.Name)
	value.WriteString("\",")
	value.WriteString(strconv.Itoa(s.Age))
	value.WriteString(",")
	value.WriteString(strconv.FormatFloat(s.Heartbeat, 'f', 6, 64))
	return value.String()
}

func createTDengineRepository() (ITDengineRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	return mapping.GetTDengineRepository(&TDenginTest{})
}

func Mapping()(*TDengineContent,error) {
	content := DataBaseMapping("Mapping", func(mappingName string) map[string]string {
		config := make(map[string]string, 15)
		config["ipport"] = "192.168.1.34:6041"
		config["username"] = "root"
		config["password"] = "taosdata"
		config["dbname"] = "test_time_3"
		config["keepday"] = "365"  //保存天数
		config["precision"] = "us" //ms 表示毫秒，us 表示微秒，ns 表示纳秒，默认 ms 毫秒
		return config
	}, func() []ITDengineTable {
		res := make([]ITDengineTable, 1)
		res[0] = &TDenginTest{}
		return res
	})
	return content, nil
}