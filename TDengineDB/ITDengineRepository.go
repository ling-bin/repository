package TDengineDB

//ITDengineRepository 仓储
type ITDengineRepository interface {
	// GetDatabase 获取数据库名
	GetDatabase() string

	//GetSuperTableName 获取表名称
	GetSuperTableName() string

	//Add 添加数据(单个)对单个表操作(不能存储带有特殊字符内容如:\n \r \...)
	//tableName:表名称   value:1，“A”,123   tags:"北京"，24
	Add(tableName, value, tags string) (int64, error)

	//AddMany 添加数据(单个或批量)对单个表操作(不能存储带有特殊字符内容如:\n \r \...)
	//map[ key:表名称，value:{"Tags":"tag0,tag1...",data:["\"1538548695000, 12.6, 218, 0.33\",""\"1538548695000, 12.6, 218, 0.33\"...]}} ] values 最大长度不能超过 数据库配置值(数据库默认大小62k)
	AddMany(dataDict map[string]*InsertData) (int64, error)

	//Execute 直接执行sqlCmd，可查询，可插入,得到影响结果(不能存储带有特殊字符内容如:\n \r \...)
	Execute(sqlCmd string) (*ExecuteResult,error)

	// QueryCount 查询总个数
	// queryWhere:条件(a='123')
	QueryCount(queryWhere string) (int64, error)

	// QuerySuperTop 根据条件查询超级中的数据
	// columns:列名,...（*或""表示全部)
	// queryWhere:条件(a='123')；
	// topCount: 排序后前多少条；
	// sort排序(只支持时间): 0 => 默认,1 => order by ts desc， 2 => order by ts；
	QuerySuperTop(columns, queryWhere string, topCount, sort int) (result *ExecuteResult, err error)

	// QuerySuperPage 根据条件查询超级中的数据
	// columns:列名,...（*或""表示全部）;
	// queryWhere:条件(a='123')；
	// pageIndex:页数从1开始；
	// pageSize:页条数
	// sort排序(只支持时间): 0 => 默认,1 => order by ts desc， 2 => order by ts；
	QuerySuperPage(columns, queryWhere string, pageIndex, pageSize, sort int) (result *ExecuteResult, totalCount int64, err error)

	// QuerySuper 根据条件查询超级中的数据
	// columns:列名,...（*或""表示全部）;
	// queryWhere:条件(a='123')；
	// pageIndex:页数从1开始；
	// pageSize:页条数
	// sort排序(只支持时间): 0 => 默认,1 => order by ts desc， 2 => order by ts；
	// isTotalCount:是否需要总数
	QuerySuper(columns, queryWhere string, pageIndex, pageSize, sort int, isTotalCount bool) (result *ExecuteResult, totalCount int64, err error)
}

