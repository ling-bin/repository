package mgoDB

import (
	"errors"
	"fmt"
	"gitee.com/ling-bin/go-utils/idCounter"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
	"time"
)
//测试结构体
type TestMog struct {
	Id   string `bson:"_id"`
	Name string `bson:"name"` //名称
	Age  int    `bson:"age"`  //年龄

}

var id = idCounter.NewObjectID().Hex()
func TestMgoRepository_Insert(t *testing.T) {
	fmt.Println("[Insert]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	id, err := repository.Add(&TestMog{
		Id:  id ,
		Name: "测试",
		Age:  20,
	})
	if err != nil {
		panic(fmt.Sprint("插入数据异常:", err))
		return
	}
	fmt.Println("插入数据id:",id)
	fmt.Println("[Insert]--------------------------------------")
}

func TestMgoRepository_Update(t *testing.T) {
	fmt.Println("[Update]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	result := &TestMog{}
	err = repository.FindById(id,nil, result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据:", result)
	upName := map[string]interface{}{
		"name": fmt.Sprint("修改测试:", time.Now().Unix()),
		"age":  5,
	}
	count, err := repository.Set("_id", id, upName)
	if err == nil{
		fmt.Sprint("更新数据条数:", count)
	}
	err = repository.FindById(id,nil, result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据:", result)
	fmt.Println("[Update]--------------------------------------")
}

func TestMgoRepository_FindById(t *testing.T) {
	fmt.Println("[FindById]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	result := &TestMog{}

	err = repository.FindById("616f7e4f24c14d20d072ba48",[]string{"name","age"}, result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据:", result)
	fmt.Println("[FindById]--------------------------------------")
}

func TestMgoRepository_FindByIds(t *testing.T) {
	fmt.Println("[FindByIds]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	ids := []interface{}{"6181f1d224c14d33a896e260","616f7e4f24c14d20d072ba48"}
	result := make([]*TestMog,0,10)
	//err = repository.FindByIds(ids,[]string{"name"}, &result)
	err = repository.FindByIds(ids,nil, &result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据:")
	for _,val:=range result{
		fmt.Println(val)
	}
	fmt.Println("[FindByIds]--------------------------------------")
}

func TestMgoRepository_FindMany(t *testing.T) {
	fmt.Println("[FindMany]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	result := make([]*TestMog,0,10)
	err = repository.FindMany(map[string]interface{}{"_id":"616f7e4f24c14d20d072ba48"},[]string{"name"}, &result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据:")
	for _,val:=range result{
		fmt.Println(val)
	}
	fmt.Println("[FindMany]--------------------------------------")
}


func TestMgoRepository_FindPage(t *testing.T) {
	fmt.Println("[FindPage]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	result := make([]*TestMog, 0, 10)
	count, err := repository.FindPage(nil, 1, 10, "_id", -1, []string{"name"}, &result, true)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据个数：", count)
	fmt.Println("获取数据:")
	for _,val:=range result{
		fmt.Println(val)
	}
	fmt.Println("[FindPage]--------------------------------------")
}

func TestMgoRepository_Delete(t *testing.T) {
	fmt.Println("[Delete]--------------------------------------")
	repository, err := createTestMogRepository()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	count, err := repository.Remove(id)
	if err != nil {
		panic(fmt.Sprint("删除数据异常:", err))
		return
	}

	fmt.Println("删除数据条数:", count, "条")
	fmt.Println("[Delete]--------------------------------------")
}


func TestMgoRepository_FindPage_2(t *testing.T) {
	fmt.Println("[FindPage]--------------------------------------")
	repository, err := createtestmogrepository2()
	if err != nil {
		panic(fmt.Sprint("mongodb，数据库连接异常:", err))
		return
	}
	result := make([]*GpsUser, 0, 10)
	count, err := repository.FindPage(nil, 1, 10, "_id", -1, []string{"macid"}, &result, false)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}

	guid := &idCounter.Guid{}
	guid.UnmarshalJSON(result[0].Id.Data)

	fmt.Println(guid.Hex())

	fmt.Println("获取数据个数：", count)
	fmt.Println("获取数据:")
	for _,val:=range result{
		fmt.Println(val)
	}
	fmt.Println("[FindPage]--------------------------------------")
}

//测试结构体
type GpsUser struct {
	Id      primitive.Binary `bson:"_id"`
	MacId   string           `bson:"macid"` //名称
	Monthly float64          `bson:"monthly"`
}

//创建用户仓储
func createtestmogrepository2() (IMgoRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	// 部门索引
	collectionName := "GpsUser1"
	repository,ok := mapping.GetMgoRepository(collectionName)
	if !ok {
		return nil, errors.New(fmt.Sprint("获取数据表",collectionName,"异常"))
	}
	return repository,nil
}


//创建用户仓储
func createTestMogRepository() (IMgoRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	collectionName := "TestMog"
	repository,ok := mapping.GetMgoRepository(collectionName)
	if !ok {
		return nil, errors.New(fmt.Sprint("获取数据表",collectionName,"异常"))
	}
	return repository,nil
}

func Mapping()(*MgoContent,error) {
	// 获取mongo实例连接
	database, err := DataBaseMapping("Mapping", func(mappingName string) map[string]string {
		//配置
		config := make(map[string]string, 5)
		config["ipport"] = "192.168.1.24:27017"
		config["dbname"] = "DesnVmsAccount1"
		config["timeout"] = "20"  //秒
		return config
	}, func() map[string][]IndexItem {
		//索引映射配置
		res := map[string][]IndexItem{
			"TestMog": {
				IndexItem{Elems: []string{"name"}, Unique: false, Desc: false},
			},
			"GpsUser1": {
				IndexItem{Elems: []string{"name"}, Unique: false, Desc: false},
			},
		}
		return res
	})
	if err != nil {
		return nil, errors.New(fmt.Sprint("获取数据库异常:", err))
	}
	return database, nil
}


