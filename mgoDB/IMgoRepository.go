package mgoDB

// IMgoRepository 仓储接口
type IMgoRepository interface {
	//Add 插入单个
	// entity  结构体
	Add(entity interface{}) (id interface{}, err error)

	//AddMany 插入多个
	// entity 结构体切片
	AddMany(entity ...interface{}) (ids []interface{}, err error)

	//Remove 删除单个
	// id 主键
	Remove(id ...interface{}) (count int64, err error)

	// Set filterKey   过滤的字段名称[与标签一致]
	// filterValue 过滤的字段值
	// projection  要查询的字段
	// entity      更新的结构体（支持局部更新）
	Set(filterKey string, filterValue interface{}, entity interface{}) (count int64, err error)

	//FindById 单条件查询单个[id]
	// filterValue 过滤的字段值
	// filterField 过滤需要的字段,nil 为全部 ，[]string{ “字段1”，“字段2”...}
	// result      查询的结果（指针）
	FindById(id interface{},filterField []string, result interface{}) (err error)

	//FindByIds 单条件查询单个[id]
	// filterValue 过滤的字段值
	// filterField 过滤需要的字段,nil 为全部 ，[]string{ “字段1”，“字段2”...}
	// result      查询的结果（指针）
	FindByIds(ids []interface{},filterField []string, result interface{}) (err error)

	//Find 单条件查询
	// filterKey   过滤的字段名称[与标签一致]
	// filterValue 过滤的字段值
	// filterField 过滤需要的字段,nil 为全部 ，[]string{ “字段1”，“字段2”...}
	// result      查询的结果
	Find(filterKey string, filterValue interface{},filterField []string, result interface{}) (err error)

	//FindMany 多条件查询
	// filterMap   过滤的条件集合[与标签一致]
	// filterField 过滤需要的字段,nil 为全部 ，[]string{ “字段1”，“字段2”...}
	// result      查询的结果
	FindMany(filterMap map[string]interface{},filterField []string, result interface{}) (err error)

	//FindCount 获取总数
	// filterMap   过滤的条件集合
	FindCount(filterMap map[string]interface{}) (count int64, err error)

	//FindPage 查询分页
	// filterMap 查询条件[与标签一致]
	// pageIndex  页数（从1开始）
	// size 获取个数
	// sortName 排序字段
	// desc 是否倒序(1为正序，-1为倒序) 1 为最初时间读取 ， -1 为最新时间读取
	// filterField 过滤需要的字段,nil 为全部 ，[]string{ “字段1”，“字段2”...}
	// result 查询结果(结构体切片)
	// isTotal 总数
	FindPage(filterMap map[string]interface{}, pageIndex, size int64, sortName string, desc int,filterField []string, result interface{}, isTotal bool) (totalCount int64, err error)
}
