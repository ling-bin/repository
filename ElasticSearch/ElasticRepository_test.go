package ElasticSearch

import (
	"errors"
	"fmt"
	"github.com/olivere/elastic/v7"
	"strconv"
	"testing"
)

func TestElasticRepository_Insert(t *testing.T) {
	fmt.Println("======插入开始=====")
	repository, err := createTestElasticRepository()
	if err != nil {
		panic(fmt.Sprint("数据插入，连接数据库异常：", err))
		return
	}
	for i := 0; i < 10; i++ {
		entity := &TestElastic{
			Id:   i,
			Name: fmt.Sprint("测试插入数据",i),
			Age:  10 * i,
		}
		res, err := repository.Add(strconv.Itoa(i), entity)
		if err != nil {
			panic(fmt.Sprint("数据插入，数据插入异常：", err))
			return
		}
		fmt.Println(res)
	}
	fmt.Println("======插入结束=====")
}


func TestElasticRepository_InsertMany(t *testing.T) {
	fmt.Println("======【批量】插入开始=====")
	repository, err := createTestElasticRepository()
	if err != nil {
		panic(fmt.Sprint("【批量】数据插入，连接数据库异常：", err))
		return
	}
	dataMap := make(map[string]interface{},10)
	for i := 0; i < 10000; i++ {
		entity := &TestElastic{
			Id:   i,
			Name: fmt.Sprint("【批量】测试插入数据",i),
			Age:  10 * i,
		}
		dataMap[strconv.Itoa(i)] = entity
	}
	many, err := repository.AddMany(dataMap)
	if err != nil{
		panic(fmt.Sprint("【批量】数据插入结束异常：",err))
		return
	}
	fmt.Println(len(many.Items))
	fmt.Println("======【批量】插入结束=====")
}


func TestElasticRepository_UpdateMany(t *testing.T) {
	fmt.Println("======【批量】更新开始=====")
	repository, err := createTestElasticRepository()
	if err != nil {
		panic(fmt.Sprint("【批量】数据插入，连接数据库异常：", err))
		return
	}
	dataMap := make(map[string]interface{},10)
	for i := 0; i < 10; i++ {
		entity := &TestElastic{
			Id:   i,
			Name: fmt.Sprint("【批量】测试更新数据",i),
			Age:  10 * i,
		}
		dataMap[strconv.Itoa(i)] = entity
	}
	many, err := repository.SetMany(dataMap)
	if err != nil{
		panic(fmt.Sprint("【批量】数据更新结束异常：",err))
		return
	}
	fmt.Println(len(many.Items))
	fmt.Println("======【批量】插入结束=====")
}

func TestElasticRepository_Update(t *testing.T) {
	fmt.Println("======更新开始=====")
	repository, err := createTestElasticRepository()
	if err != nil{
		panic(fmt.Sprint("数据更新，连接数据库异常：",err))
		return
	}
	res, err := repository.Set("1", map[string]interface{}{
		"Name":"更新数据测试",
	})
	if err != nil{
		panic(fmt.Sprint("数据更新异常：",err))
		return
	}
	fmt.Println(res)
	fmt.Println("======更新结束=====")
}

func TestElasticRepository_GetById(t *testing.T) {
	fmt.Println("======根据ID获取数据开始=====")
	repository, err := createTestElasticRepository()
	if err != nil{
		panic(fmt.Sprint("连接数据库异常：",err))
		return
	}
	entity := &TestElastic{}
	err = repository.GetById("1",entity)
	if err != nil{
		panic(fmt.Sprint("数据根据ID获取数据异常：",err))
		return
	}
	fmt.Println(entity)
	fmt.Println("======根据ID获取数据结束=====")
}

func TestElasticRepository_Delete(t *testing.T) {
	fmt.Println("======删除开始=====")
	repository, err := createTestElasticRepository()
	if err != nil{
		panic(fmt.Sprint("连接数据库异常：",err))
		return
	}
	res, err := repository.Remove("1","2")
	if err != nil{
		panic(fmt.Sprint("数据删除异常：",err))
		return
	}
	fmt.Println(res)
	fmt.Println("======删除结束=====")
}

func TestElasticRepository_Query(t *testing.T) {
	fmt.Println("======查询开始=====")
	repository, err := createTestElasticRepository()
	if err != nil{
		panic(fmt.Sprint("连接数据库异常：",err))
		return
	}
	//字段相等
	query := elastic.NewQueryStringQuery("name:测试插入数据0")
	entity, err := repository.Query(query)
	if err != nil {
		panic(fmt.Sprint("数据查询异常：",err))
		return
	}
	fmt.Println(entity)
	fmt.Println("======查询结束=====")
}

func TestElasticRepository_Search(t *testing.T) {
	fmt.Println("======搜索开始=====")
	repository, err := createTestElasticRepository()
	if err != nil{
		panic(fmt.Sprint("连接数据库异常：",err))
		return
	}
	//年龄大于30岁的
	//boolQ := elastic.NewBoolQuery()
	//boolQ.Must(elastic.NewMatchQuery("last_name", "smith"))
	//boolQ.Filter(elastic.NewRangeQuery("age").Gt(30))
	query := elastic.NewMatchPhraseQuery("name", "测试更新数据")
	entity, count, err := repository.Search(query)
	if err != nil {
		panic(fmt.Sprint("数据搜索异常：",err))
		return
	}
	fmt.Println("分页后总数：", count)
	fmt.Println("当前页个数：",len(entity))
	fmt.Println("======搜索分页结束=====")
}

func TestElasticRepository_SearchPage(t *testing.T) {
	fmt.Println("======搜索分页开始=====")
	repository, err := createTestElasticRepository()
	if err != nil {
		panic(fmt.Sprint("连接数据库异常：", err))
		return
	}
	//字段相等
	//query := elastic.NewQueryStringQuery("name:测试更新数据")

	query := elastic.NewMatchPhraseQuery("name", "测试插入数据")
	entity, count, err := repository.SearchPage(query, 1, 50)
	if err != nil {
		panic(fmt.Sprint("数据搜索分页异常：", err))
		return
	}
	fmt.Println("分页后总数：", count)
	fmt.Println("当前页个数：",len(entity))
	fmt.Println("======搜索分页结束=====")
}

//测试结构体
type TestElastic struct {
	Id   int    `json:"id"`
	Name string `json:"name"` //名称
	Age  int    `json:"age"`  //年龄
}

//创建用户仓储
func createTestElasticRepository() (IElasticRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	repository, ok := mapping.GetElasticRepository(&TestElastic{})
	if !ok {
		return nil, errors.New(fmt.Sprint("获取数据表异常"))
	}
	return repository, nil
}

//映射
func Mapping()(*ElasticContent,error) {
	return DataBaseMapping("Mapping", func(mappingName string) map[string]string {
		//配置
		config := make(map[string]string, 5)
		config["ipport"] = "http://127.0.0.1:9200"
		config["dbname"] = "elastictest"
		config["timeout"] = "30" //秒
		return config
	}, func() []interface{} {
		//表映射
		res := make([]interface{}, 1)
		res[0] = &TestElastic{}
		return res
	})
}