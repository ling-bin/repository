package ElasticSearch

import "github.com/olivere/elastic/v7"

//IElasticRepository 搜索引擎仓储接口
type IElasticRepository interface {

	//Add 插入单个
	// id id值
	// entity  结构体
	Add(id string, entity interface{}) (res *elastic.IndexResponse,err error)

	//AddMany 插入多个
	// entityMap【id值】结构体
	AddMany(entityMap map[string]interface{}) (res *elastic.BulkResponse,err error)

	//Remove 删除
	// ids id值
	Remove(id ...string) (*elastic.BulkResponse, error)

	//Set 更新
	//id     文档id
	//doc    更新内容
	Set(id string, doc map[string]interface{}) (res *elastic.UpdateResponse, err error)

	//SetMany 更新多个
	// entityMap【id值】结构体
	SetMany(entityMap map[string]interface{}) (res *elastic.BulkResponse,err error)

	//GetById 查询单个
	// id         id值
	// entity      查询的结果
	GetById(id string, result interface{}) error

	//Query 查询
	//query  查询条件
	//返回 搜索结果不处理
	Query(query elastic.Query)(result *elastic.SearchResult,err error)

	//Search 搜索
	// query  查询条件
	// 搜索返回具体结果
	Search(query elastic.Query) (entity []interface{},totalCount int64, err error)

	//SearchPage 搜索结果分页显示((pageSize-1)*pageIndex <= 10000,大于则获取不到内容)
	//query      查询条件
	//pageIndex  当前页，从1开始
	//pageSize   每页多少条
	//entity     当前页详细数据
	//totalCount 页总数
	SearchPage(query elastic.Query,pageIndex,pageSize int) (entity []interface{},totalCount int64,err error)
}