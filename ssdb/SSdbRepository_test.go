package ssdb

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/seefan/gossdb/v2/pool"
	"strconv"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

var key = "densSSDBTest"
func TestGoSSDBRepository_Set(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil{
		panic(fmt.Sprint("数据库连接异常！"))
		return
	}
	count, err := repository.Set(key,&TestRed{
		Id:   time.Now().Unix() ,
		Name: "测试",
		Age:  20,
	},0)
	if err != nil {
		panic(fmt.Sprint("插入数据异常:", err))
		return
	}
	fmt.Println("插入数据条数:",count,"条")
}

func TestGoSSDBRepository_Incr(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常！"))
		return
	}
	incr, err := repository.Incr(key, "count", 1)
	if err != nil {
		panic(fmt.Sprint("递增数据异常:", err))
		return
	}
	fmt.Println("递增后值:", incr, "条")
}

func TestGoSSDBRepository_GetIncr(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常！"))
		return
	}
	res := map[string]int64{
		"count": 0,
	}
	err = repository.GetIncr(key, res)
	if err != nil {
		panic(fmt.Sprint("获取递增数据异常:", err))
		return
	}
	fmt.Println("获取递增数据:")
	fmt.Println(res)
}

func TestGoSSDBRepository_RemoveIncr(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	err = repository.RemoveIncr(key)
	if err != nil {
		panic(fmt.Sprint("移除递增数据异常:", err))
		return
	}
	fmt.Println("递增已经移除")
	res := map[string]int64{
		"count": 0,
	}
	err = repository.GetIncr(key, res)
	if err != nil {
		panic(fmt.Sprint("获取递增数据异常:", err))
		return
	}
	repository.Do(func(client *pool.Client) {
		
	})
	fmt.Println("获取递增数据:")
	fmt.Println(res)
}

func TestGoSSDBRepository_Get(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	result := &TestRed{}
	err = repository.Get(key, result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据单个:", result)
}

func TestGoSSDBRepository_SetMany(t *testing.T) {
	count := 20
	var wg = sync.WaitGroup{}
	wg.Add(count)
	totalCount := int64(0)
	for z := 0 ; z < count;z++ {
		go func(z int) {
			for index := 0; index < 1000 ; index++ {
				repository, err := createTestGoSSDBRepository()
				if err != nil {
					panic(fmt.Sprint("数据库连接异常:", err))
					return
				}
				dataMap := make(map[string]interface{})
				for i := 0; i < 1000; i++ {
					dataMap[fmt.Sprint(key, ":", i, ":", index, ":", z)] = &TestRed{
						Id:   time.Now().Unix(),
						Name: "测试:" + strconv.Itoa(i)+":"+ strconv.Itoa(index)+":"+ strconv.Itoa(z),
						Age:  20,
					}
				}
				result, err := repository.SetMany(dataMap)
				if err != nil {
					panic(fmt.Sprint("设置数据异常:", err))
					return
				}
				//fmt.Println("设置数据多个:", result)
				atomic.AddInt64(&totalCount,result)
			}
			wg.Done()
		}(z)
	}
	wg.Wait()
	fmt.Println("完成设置数据共个:", totalCount)
}


func TestGoSSDBRepository_List(t *testing.T) {
	count := 20
	var wg = sync.WaitGroup{}
	wg.Add(count)
	totalCount := int64(0)
	for z := 0 ; z < count;z++ {
		go func(z int) {
			dCount := 10000
			repository, err := createTestGoSSDBRepository()
			dataS := make([]interface{},0,dCount)
			for index := 0; index < dCount ; index++ {
				jsonStr,_ := json.Marshal(&TestRed{
					Id:   time.Now().Unix(),
					Name: fmt.Sprint("测试:" ,time.Now().Unix()),
					Age:  20,
				})
				dataS = append(dataS, jsonStr)
				//fmt.Println("设置数据多个:", result)
			}
			if err != nil {
				panic(fmt.Sprint("数据库连接异常:", err))
				return
			}
			key := fmt.Sprint("0456488797878978:",z)
			repository.Do(func(client *pool.Client) {
				client.Get(key)
			})
			atomic.AddInt64(&totalCount,int64(dCount))
			wg.Done()
		}(z)
	}
	wg.Wait()
	fmt.Println("完成设置数据共个:", totalCount)
}

func TestGoSSDBRepository_GetMany(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	result := make([]*TestRed, 0, 10)
	err = repository.GetMany([]string{key,fmt.Sprint(key,1000),""}, &result)
	if err != nil {
		panic(fmt.Sprint("获取数据异常:", err))
		return
	}
	fmt.Println("获取数据多个:", len(result))
}

func TestGoSSDBRepository_Remove(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	count, err := repository.Remove(key)
	if err != nil {
		panic(fmt.Sprint("删除数据异常:", err))
		return
	}
	fmt.Println("删除数据条数:", count, "条")
}

func TestGoSSDBRepository_GetClient(t *testing.T) {
	repository, err := createTestGoSSDBRepository()
	if err != nil {
		panic(fmt.Sprint("数据库连接异常:", err))
		return
	}
	repository.Do(func(client *pool.Client) {
		
	})
	if err != nil {
		panic(fmt.Sprint("获取大小异常：", err))
		return
	}
}

//创建用户仓储
func createTestGoSSDBRepository() (ISSdbRepository, error) {
	mapping, err := Mapping()
	if err != nil {
		return nil, err
	}
	repository,ok := mapping.GetGoSSDBRepository("TestRed")
	if !ok {
		return nil,errors.New("获取表异常")
	}
	return repository, nil
}

//映射
func Mapping()(*GoSSDBContent,error) {
	// 获取mongo实例连接
	return DataBaseMapping("Mapping", func(mappingName string) map[string]string {
		config := make(map[string]string, 5)
		config["ipport"] = "127.0.0.1:8889"
		config["timeout"] = "30"  //秒
		return config
	}, func() map[string]int {
		return map[string]int{
			"TestRed":1,
		}
	})
}

//测试结构体
type TestRed struct {
	Id   int64  `json:"id"`
	Name string `json:"name"` //名称
	Age  int    `json:"age"`  //年龄
}