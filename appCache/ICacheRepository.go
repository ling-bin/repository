package appCache

import "time"

//ICacheRepository 缓存接口
type ICacheRepository interface {

	//GetOrAddDuration 获取或添加 滑动到期时间
	//key 缓存key
	//foundCall 数据不存在时缓存函数
	//duration 滑动到期时间
	GetOrAddDuration(key string, foundCall func(key string) interface{}, duration time.Duration) (entity interface{}, err error)

	//GetOrAdd 获取或添加固定到期时间可设置提前回调时间
	//key 缓存key
	//foundCall 数据不存在时缓存函数
	//ttl  到期时间单位 秒
	//expireFrontCall 到期前回调函数
	//frontNotice  提前到期通知
	GetOrAdd(key string, foundCall func(key string) interface{}, ttl int, expireFrontCall func(interface{}, int64), frontNotice int) (entity interface{}, err error)

	//Get 获取缓存
	//key 缓存key
	//entity  缓存数据
	Get(key string) (entity interface{}, err error)

	//Remove 移除缓存
	//key 缓存key (由外部业务保证唯一性)
	Remove(key string)

	//SetDuration 设置缓存  滑动到期
	//key 缓存key
	//entity  缓存数据
	//duration 滑动到期时间
	SetDuration(key string, entity interface{}, duration time.Duration)

	//Set     设置缓存  固定到期
	//key     缓存key
	//entity  缓存数据
	//ttl     到期时间单位 秒
	//expireFrontCall 到期前回调函数
	//frontNotice  提前到期通知
	Set(key string, entity interface{}, ttl int, expireFrontCall func(interface{}, int64), frontNotice int)
}
