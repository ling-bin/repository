package appCache

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)
var(
	repository = createTestCacheRepository()
)
// go test -bench=. -benchmem -run=none
func BenchmarkGetCacheRepository(b *testing.B) {
	fmt.Println(b.N)
	for i := 0; i < b.N; i++ {
		keyStr := strconv.Itoa(i)
		test := &TestRed{
			Id:   keyStr,
			Name: "测试",
			Age:  10,
		}
		repository.Set(keyStr, test, 200, func(i interface{}, i2 int64) {

		}, 0)
		obj, err := repository.Get(keyStr)
		gTest := obj.(*TestRed)
		if gTest.Id != test.Id {
			panic("获取和设置不一致")
		}
		if err != nil {
			panic(err)
		}
		repository.Remove(keyStr)
	}
}

func TestCacheRepository_SetDuration(t *testing.T) {
	for i := 0; i < 10; i++ {
		keyStr := fmt.Sprint(i)
		repository.SetDuration(keyStr, &TestRed{
			Id:   keyStr,
			Name: "测试",
			Age:  10,
		}, time.Second*10)
	}
	fmt.Println("设置完成")
	time.Sleep(time.Second * 2)
	repository.Get("1")
	fmt.Println("获取，访问延期")
	time.Sleep(time.Second * 9)
	_, err := repository.Get("1")
	if err != nil{
		fmt.Println("空")
	}else {
		fmt.Println("有值，访问延期")
	}
	time.Sleep(time.Second * 10)
	_, err = repository.Get("1")
	if err != nil{
		fmt.Println("空")
	}else {
		fmt.Println("有值")
	}
}

func TestCacheRepository_Set(t *testing.T) {
	for i := 0; i < 10; i++ {
		keyStr := fmt.Sprint(i)
		repository.Set(keyStr, &TestRed{
			Id:   keyStr,
			Name: "测试",
			Age:  10,
		}, 10, func(i interface{}, i2 int64) {
			fmt.Println("第一回调：",i)
		}, 2)
	}
	for i := 0; i < 10; i++ {
		keyStr := fmt.Sprint(i)
		repository.Set(keyStr, &TestRed{
			Id:   keyStr,
			Name: "测试",
			Age:  10,
		}, 10, func(i interface{}, i2 int64) {
			fmt.Println("第二回调：",i)
		}, 2)
	}
	fmt.Println("设置完成")
	for {
		time.Sleep(time.Second * 1)
	}
}

//测试结构体
type TestRed struct {
	Id   string `json:"id"`
	Name string `json:"name"` //名称
	Age  int    `json:"age"`  //年龄
}


//创建用户仓储
func createTestCacheRepository() ICacheRepository {
	return GetCacheRepository("TestRed")
}
