package appCache

import (
	"errors"
	"gitee.com/ling-bin/go-utils/timingwheel"
	"sync"
	"sync/atomic"
	"time"
)

var(
	lock sync.Mutex
	_CacheId int64
	cachePoolMap = sync.Map{}
	timingWhee   = timingwheel.NewTimingWheel(time.Second, 60)      //初始化秒级多层时间轮
	notfound     = errors.New(" key not found!")
)

//初始化
func init() {
	timingWhee.Start()
}

//GetCacheRepository 获取缓存仺储
func GetCacheRepository(entityName string) ICacheRepository {
	if val, ok := cachePoolMap.Load(entityName); ok {
		return val.(ICacheRepository)
	}

	lock.Lock()
	defer lock.Unlock()

	if val, ok := cachePoolMap.Load(entityName); ok {
		return val.(ICacheRepository)
	}
	repository := &CacheRepository{
		entityName: entityName,
		cacheMap:   sync.Map{},
	}
	cachePoolMap.Store(entityName, repository)
	return repository
}

//CacheRepository 缓存仓储
type CacheRepository struct {
	entityName string
	cacheMap   sync.Map
}

//GetOrAddDuration 获取没有就添加
func (c *CacheRepository) GetOrAddDuration(key string,foundCall func(key string) interface{}, duration time.Duration) (entity interface{},err error) {
	result, ok := c.cacheMap.Load(key)
	if !ok {
		result = foundCall(key)
		c.SetDuration(key, result, duration)
		return result, nil
	}
	cacheItem := result.(*cacheItem)
	ctime := time.Now()
	if ctime.Sub(cacheItem.expireTime).Seconds() > 0 {
		return nil, notfound
	}
	if cacheItem.duration != 0 {
		cacheItem.expireTime = time.Now().Add(cacheItem.duration)
	}
	cacheItem.count++
	return cacheItem.value, nil
}

//GetOrAdd 获取没有就添加
func (c *CacheRepository) GetOrAdd(key string,foundCall func(key string) interface{},ttl int, expireFrontCall func(interface{},int64),frontNotice int) (entity interface{},err error) {
	result, ok := c.cacheMap.Load(key)
	if !ok {
		result = foundCall(key)
		c.Set(key, result, ttl, expireFrontCall, frontNotice)
		return result, nil
	}
	cacheItem := result.(*cacheItem)
	ctime := time.Now()
	if ctime.Sub(cacheItem.expireTime).Seconds() > 0 {
		return nil, notfound
	}
	if cacheItem.duration != 0 {
		cacheItem.expireTime = time.Now().Add(cacheItem.duration)
	}
	cacheItem.count++
	return cacheItem.value, nil
}

//Get 获取单个
//key 数据唯一key
func (c *CacheRepository) Get(key string)(entity interface{},err error) {
	result, ok := c.cacheMap.Load(key)
	if !ok {
		return nil, notfound
	}
	cacheItem := result.(*cacheItem)
	ctime := time.Now()
	if ctime.Sub(cacheItem.expireTime).Seconds() > 0 {
		return nil, notfound
	}
	if cacheItem.duration != 0 {
		cacheItem.expireTime = time.Now().Add(cacheItem.duration)
	}
	cacheItem.count++
	return cacheItem.value, nil
}

//Remove 删除单个
// key key值
func (c *CacheRepository) Remove(key string) {
	value, loaded := c.cacheMap.LoadAndDelete(key)
	if loaded {
		cacheItem := value.(*cacheItem)
		cancelTask(cacheItem)
	}
}

// SetDuration 插入单个
// key 数据唯一key
// entity  结构体
// duration 滑动到期时间,访问延期
func (c *CacheRepository) SetDuration(key string, entity interface{}, duration time.Duration) {
	id := atomic.AddInt64(&_CacheId, 1)
	var newCacheItem *cacheItem
	load, ok := c.cacheMap.Load(key)
	if ok {
		newCacheItem = load.(*cacheItem)
		newCacheItem.value = entity
		newCacheItem.id = id
		newCacheItem.duration = duration
		newCacheItem.expireTime = time.Now().Add(duration)
		newCacheItem.expireFrontCall = nil
		newCacheItem.frontNotice = 0
		newCacheItem.count = 0
		cancelTask(newCacheItem)
	}else {
		newCacheItem = &cacheItem{
			key:             key,
			value:           entity,
			duration:        duration,
			expireTime:      time.Now().Add(duration),
			expireFrontCall: nil,
			frontNotice:     0,
			count:           0,
			id:              id,
		}
		c.cacheMap.Store(key, newCacheItem)
	}
	expTime := newCacheItem.expireTime
	newCacheItem.clearTask = timingWhee.AfterTimeFunc(expTime, func() {
		c.clearSlide(key, id, expTime)
	})
}

//清理滑动
func (c *CacheRepository) clearSlide(key string,id int64, odExpTime time.Time) {
	result, ok := c.cacheMap.Load(key)
	if !ok {
		return
	}
	cacheItem := result.(*cacheItem)
	if cacheItem.id != id {
		return
	}
	//有访问延期情况
	if cacheItem.expireTime.Sub(odExpTime).Seconds() > 0 {
		expTime := cacheItem.expireTime
		cacheItem.clearTask = timingWhee.AfterTimeFunc(cacheItem.expireTime, func() {
			c.clearSlide(key, id, expTime)
		})
		//fmt.Println("滑动")
		return
	}
	c.cacheMap.Delete(key)
	//fmt.Println("清理")
}



// Set 插入单个
// key 数据唯一key
// entity  结构体
// ttl     单位秒 ，0为永不到期
// expireFrontCall  到期前回调
// frontNotice  提前回调时间 秒
func (c *CacheRepository) Set(key string, entity interface{}, ttl int, expireFrontCall func(interface{},int64),frontNotice int) {
	ttlSecond := time.Second * time.Duration(ttl)
	id := atomic.AddInt64(&_CacheId, 1)
	var newCacheItem *cacheItem
	load, ok := c.cacheMap.Load(key)
	if ok {
		newCacheItem = load.(*cacheItem)
		newCacheItem.value = entity
		newCacheItem.id = id
		newCacheItem.duration = 0
		newCacheItem.expireTime = time.Now().Add(ttlSecond)
		newCacheItem.expireFrontCall = expireFrontCall
		newCacheItem.frontNotice = frontNotice
		newCacheItem.count = 0
		cancelTask(newCacheItem)
	} else {
		newCacheItem = &cacheItem{
			key:             key,
			value:           entity,
			duration:        0,
			expireTime:      time.Now().Add(ttlSecond),
			expireFrontCall: expireFrontCall,
			frontNotice:     frontNotice,
			count:           0,
			id:              id,
		}
		c.cacheMap.Store(key, newCacheItem)
	}
	newCacheItem.clearTask = timingWhee.AfterTimeFunc(newCacheItem.expireTime, func() {
		c.clear(key, id)
	})
	if newCacheItem.expireFrontCall != nil {
		expire := newCacheItem.expireTime.Add(-(time.Second * time.Duration(frontNotice)))
		newCacheItem.callTask = timingWhee.AfterTimeFunc(expire, func() {
			c.frontCall(key, id)
		})
	}
}
//frontCall 提前通知
func (c *CacheRepository) frontCall(key string,id int64)  {
	defer func() {
		if r := recover(); r != nil {
			return
		}
	}()
	result, ok := c.cacheMap.Load(key)
	if !ok {
		return
	}
	cacheItem := result.(*cacheItem)
	if cacheItem.id != id {
		return
	}
	cacheItem.expireFrontCall(cacheItem.value, id)
	//fmt.Println("通知")
}

//clear 清理
func (c *CacheRepository) clear(key string,id int64) {
	result, ok := c.cacheMap.Load(key)
	if !ok {
		return
	}
	cacheItem := result.(*cacheItem)
	if cacheItem.id != id {
		return
	}
	c.cacheMap.Delete(key)
	//fmt.Println("清理")
}

//取消任务
func cancelTask(cacheItem *cacheItem)  {
	if cacheItem.clearTask != nil {
		cacheItem.clearTask.Stop()
		cacheItem.clearTask = nil
	}
	if cacheItem.callTask != nil {
		cacheItem.callTask.Stop()
		cacheItem.callTask = nil
	}
}

// cache 缓存
type cacheItem struct {
	key             string
	value           interface{}
	duration        time.Duration
	expireTime      time.Time
	expireFrontCall func(interface{}, int64)
	frontNotice     int
	count           int64
	id              int64
	clearTask       *timingwheel.Timer
	callTask        *timingwheel.Timer
}